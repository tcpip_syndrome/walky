package net.logcat.walkinglooking.location

import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import kotlin.system.measureTimeMillis

class LocationProviderTest {

    @Test
    fun locationEmitterContentTest() = runBlocking {
        val twentyFiveLocations = LocationProvider(0).location.take(25).toList()
        assertEquals(twentyFiveLocations.size, 25)
        assertEquals(twentyFiveLocations[0], Location(60.170187, 24.930599))
        assertEquals(twentyFiveLocations[10], Location(60.170187, 24.930599))
        assertEquals(twentyFiveLocations[20], Location(60.170187, 24.930599))
        assertEquals(twentyFiveLocations[21], Location(60.169418, 24.931618))

//        twentyFiveLocations.forEach(::println)
    }

    @Test
    fun locationEmitterIntervalTest() = runBlocking {
        val elapsedFiveLocationsOverOneSecond = measureTimeMillis { LocationProvider(1000).location.take(5).toList() }
        assert(elapsedFiveLocationsOverOneSecond in 4001..5999)
    }
}