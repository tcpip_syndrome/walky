package net.logcat.walkinglooking.api

import kotlinx.coroutines.runBlocking
import net.logcat.walkinglooking.location.Location
import org.junit.Test

class VenuesApiTest {

    @Test
    fun testGettingVenuesAtLocation() = runBlocking {
        val venues = VenuesApiBuilder.build().venuesAt(60.170187, 24.930599).venues
        assert(venues.isNotEmpty())

//        println(venues[0])
//        venues.forEach {
//            println(it.name[0])
//        }
    }

}