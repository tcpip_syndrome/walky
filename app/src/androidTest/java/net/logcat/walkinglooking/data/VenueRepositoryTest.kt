package net.logcat.walkinglooking.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import arrow.core.Either
import arrow.core.right
import kotlinx.coroutines.runBlocking
import net.logcat.walkinglooking.WalkingApp
import net.logcat.walkinglooking.location.Location
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class VenueRepositoryTest {

    @Test
    fun testVenueRepository() = runBlocking {
        val venuesRepository = WalkingApp.venuesRepository
        val venuesResult = venuesRepository.venuesShortListAt(Location(60.170187, 24.930599))
        when (venuesResult) {
            is Either.Right -> {
                val venues = venuesResult.value
                assert(venues.isNotEmpty())
                val venue = venues[0]
                assert(!venue.favorite)

                venue.favorite = true
                venuesRepository.setFavorite(this, venue)
                assert(venuesRepository.venuesDao.isFavorite(venue.venue.id()))

                venue.favorite = false
                venuesRepository.setFavorite(this, venue)
                assert(!venuesRepository.venuesDao.isFavorite(venue.venue.id()))
            }
        }
    }

}