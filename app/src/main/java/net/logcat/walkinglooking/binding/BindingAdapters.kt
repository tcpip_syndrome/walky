package net.logcat.walkinglooking.binding

import android.view.View
import android.widget.ImageView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import net.logcat.walkinglooking.R
import net.logcat.walkinglooking.util.dpToPx
import kotlin.math.roundToInt

object BindingAdapters {

    @JvmStatic
    @BindingAdapter("visibility")
    fun View.setVisibility(visibilityBool: Boolean) {
        visibility = if (visibilityBool) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    @JvmStatic
    @BindingAdapter("imageUrl")
    fun ImageView.setImageFromUrl(url: String) {
        val options = RequestOptions().transform(
            CenterCrop(),
            RoundedCorners(this.resources.dpToPx(4f).roundToInt())
        ).placeholder(R.drawable.ic_launcher_foreground)
            .error(R.drawable.ic_launcher_foreground)
        Glide.with(this).load(url).apply(options).into(this)
    }

    @JvmStatic
    @BindingAdapter("displayErrorMessage")
    fun CoordinatorLayout.displayError(errorMessageId: Int) {
        if (errorMessageId >= 0) {
            val snackbar = Snackbar.make(this, context.getString(errorMessageId), BaseTransientBottomBar.LENGTH_LONG)
            snackbar.setAction(context.getString(R.string.ok), {
                    snackbar.dismiss()
                }).show()
        }
    }
}
