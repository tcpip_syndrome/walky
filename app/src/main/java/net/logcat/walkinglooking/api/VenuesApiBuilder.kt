package net.logcat.walkinglooking.api

import net.logcat.walkinglooking.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class VenuesApiBuilder {

    companion object {
        fun build(): VenuesApi {
            return Retrofit.Builder().baseUrl(BuildConfig.VENUES_URL)
                .addConverterFactory(GsonConverterFactory.create()).build()
                .create(VenuesApi::class.java)
        }
    }
}