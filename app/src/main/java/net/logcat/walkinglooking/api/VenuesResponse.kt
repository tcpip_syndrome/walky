package net.logcat.walkinglooking.api

import com.google.gson.annotations.SerializedName
import net.logcat.walkinglooking.api.LocalizableString.Companion.findCurrentLocaleValue
import java.util.*

data class Id(
    @SerializedName("\$oid")
    val id: String
)

data class LocalizableString(
    val lang: String,
    val value: String
) {
    companion object {
        fun findCurrentLocaleValue(localizedStrings: List<LocalizableString>):String {
            val currentLang = Locale.getDefault().language
            val fallbackLang = Locale.ENGLISH.language
            var fallbackValue = ""
            for (localizedString in localizedStrings) {
                if (currentLang == localizedString.lang) {
                    return localizedString.value
                } else if (fallbackLang == localizedString.lang) {
                    fallbackValue = localizedString.value
                }
            }
            if (fallbackValue.isEmpty() && localizedStrings.isNotEmpty()) {
                fallbackValue = localizedStrings[0].value
            }
            return fallbackValue
        }
    }
}

typealias Name = LocalizableString

typealias ShortDescription = LocalizableString

data class Venue(
    val id: Id,
    val name: List<Name>,
    @SerializedName("short_description")
    val shortDescription: List<ShortDescription>,
    @SerializedName("listimage")
    val imageUrl: String
) {
    fun name(): String {
        return findCurrentLocaleValue(name)
    }
    fun shortDescription(): String {
        return findCurrentLocaleValue(shortDescription)
    }
    fun id(): String {
        return id.id
    }
}

data class VenuesResponse(
    val status: String,
    @SerializedName("results")
    val venues: List<Venue>
)
