package net.logcat.walkinglooking.api

import net.logcat.walkinglooking.location.Location
import retrofit2.http.GET
import retrofit2.http.Query

interface VenuesApi {

    @GET("venues")
    suspend fun venuesAt(
        @Query("lat")
        lat: Double,
        @Query("lon")
        lon: Double): VenuesResponse

}