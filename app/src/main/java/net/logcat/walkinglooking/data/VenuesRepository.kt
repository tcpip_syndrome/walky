package net.logcat.walkinglooking.data

import android.util.Log
import arrow.core.Either
import arrow.core.Either.Left
import arrow.core.Either.Right
import kotlinx.coroutines.*
import net.logcat.walkinglooking.api.VenuesApi
import net.logcat.walkinglooking.db.FavoriteVenueDao
import net.logcat.walkinglooking.location.Location

class VenuesRepository(val venuesApi: VenuesApi, val venuesDao: FavoriteVenueDao) {

    val LOG_TAG = VenuesRepository::class.java.simpleName

    val dbJobs = mutableListOf<Job>()

    suspend fun venuesShortListAt(location: Location): Either<Exception, List<Venue>> {
        try {
            val venuesAt = venuesApi.venuesAt(location.latitude, location.longitude)
            val venues = venuesAt.venues.take(15)

            dbJobs.joinAll()
            dbJobs.clear()

            return Right(venues.map {
                Venue(it, venuesDao.isFavorite(it.id()))
            })
        } catch (e: Exception) {
            Log.e(LOG_TAG, "error getting venues at location", e)
            return Left(e)
        }
    }

    fun setFavorite(coroutineScope: CoroutineScope, venue: Venue) {
        val job = coroutineScope.launch(Dispatchers.IO) {
            venuesDao.setFavorite(venue.venue.id())
        }
        dbJobs.add(job)
    }
}