package net.logcat.walkinglooking.data

import net.logcat.walkinglooking.api.Venue as ApiVenue

data class Venue(val venue: ApiVenue, var favorite: Boolean) {
}