package net.logcat.walkinglooking.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import net.logcat.walkinglooking.R
import net.logcat.walkinglooking.WalkingApp
import net.logcat.walkinglooking.data.Venue
import net.logcat.walkinglooking.location.LocationProvider

class VenuesViewModel : ViewModel() {

    val progressVisibility = MutableLiveData(false)
    val venues = MutableLiveData<List<Venue>>(emptyList())

    val emptyViewVisibility = Transformations.map(venues) {
        it?.isEmpty()
    }

    val errorMessage = MutableLiveData(-1)

    init {
        viewModelScope.launch(Dispatchers.IO) {
            LocationProvider().location.collect { location ->
                progressVisibility.postValue(true)
                val venuesList = WalkingApp.venuesRepository.venuesShortListAt(location)
                progressVisibility.postValue(false)
                venuesList.map {
                    errorMessage.postValue(-1)
                    venues.postValue(it)
                }.mapLeft {
                    errorMessage.postValue(R.string.error_no_internet)
                }
            }
        }
    }

}