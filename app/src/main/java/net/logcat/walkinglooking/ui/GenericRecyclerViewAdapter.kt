package net.logcat.walkinglooking.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import net.logcat.walkinglooking.BR

open class GenericRecyclerViewAdapter<T>(@LayoutRes open val itemLayoutId: Int) :
    RecyclerView.Adapter<GenericRecyclerViewAdapter.ViewHolder<T>>() {

    val itemModels = mutableListOf<T>()

    open fun setItems(items: List<T>) {
        this.itemModels.clear()
        this.itemModels.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder<T> {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            itemLayoutId,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder<T>, position: Int) {
        val itemModel = itemModels[position]
        holder.binding.setVariable(BR.viewModel, itemModel)
        holder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return itemModels.size
    }

    class ViewHolder<T>(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)

}