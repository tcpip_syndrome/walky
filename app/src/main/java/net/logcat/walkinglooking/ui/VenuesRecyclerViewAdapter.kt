package net.logcat.walkinglooking.ui

import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil

class VenuesRecyclerViewAdapter(@LayoutRes override val itemLayoutId: Int) :
    GenericRecyclerViewAdapter<VenueViewModel>(itemLayoutId) {

    override fun setItems(items: List<VenueViewModel>) {
        val diffResult = DiffUtil.calculateDiff(VenueViewModelDiffCallback(itemModels, items))
        this.itemModels.clear()
        this.itemModels.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }

    class VenueViewModelDiffCallback(
        val oldItems: List<VenueViewModel>,
        val newItems: List<VenueViewModel>
    ) : DiffUtil.Callback() {
        override fun getOldListSize(): Int = oldItems.size

        override fun getNewListSize(): Int = newItems.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldVenueViewModel = oldItems[oldItemPosition]
            val newVenueViewModel = newItems[newItemPosition]
            return oldVenueViewModel.venue.venue.id == newVenueViewModel.venue.venue.id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            val oldVenueViewModel = oldItems[oldItemPosition]
            val newVenueViewModel = newItems[newItemPosition]
            return oldVenueViewModel.venue == newVenueViewModel.venue
        }

    }
}