package net.logcat.walkinglooking.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import kotlinx.android.synthetic.main.activity_venues.*
import net.logcat.walkinglooking.R
import net.logcat.walkinglooking.databinding.ActivityVenuesBinding

class VenuesActivity : AppCompatActivity() {

    val viewModel: VenuesViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityVenuesBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_venues)
        binding.setLifecycleOwner(this)
        binding.viewModel = viewModel

        val venuesAdapter = VenuesRecyclerViewAdapter(R.layout.layout_venue_item)
        venues?.adapter = venuesAdapter

        viewModel.venues.observe(this, {
            val venueViewModels = it?.map {
                VenueViewModel(it)
            } ?: emptyList()
            venuesAdapter.setItems(venueViewModels)
        })
    }

}