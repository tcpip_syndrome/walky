package net.logcat.walkinglooking.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.logcat.walkinglooking.WalkingApp
import net.logcat.walkinglooking.data.Venue

class VenueViewModel(val venue: Venue) : ViewModel() {

    fun name(): String {
        return venue.venue.name()
    }

    fun shortDescription(): String {
        return venue.venue.shortDescription()
    }

    fun imageUrl(): String {
        return venue.venue.imageUrl
    }

    fun isFavorite(): Boolean {
        return venue.favorite
    }

    fun toggleFavorite() {
        venue.favorite = !venue.favorite
        WalkingApp.venuesRepository.setFavorite(viewModelScope, venue)
    }

}