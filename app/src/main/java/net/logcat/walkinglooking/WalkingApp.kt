package net.logcat.walkinglooking

import android.app.Application
import androidx.room.Room
import net.logcat.walkinglooking.api.VenuesApiBuilder
import net.logcat.walkinglooking.data.VenuesRepository
import net.logcat.walkinglooking.db.AppDatabase

class WalkingApp : Application() {

    override fun onCreate() {
        super.onCreate()
        venuesRepository = VenuesRepository(
            VenuesApiBuilder.build(),
            Room.databaseBuilder(
                this,
                AppDatabase::class.java, "app-database"
            ).build().favoriteVenueDao()
        )
    }

    companion object {
        lateinit var venuesRepository: VenuesRepository
    }

}