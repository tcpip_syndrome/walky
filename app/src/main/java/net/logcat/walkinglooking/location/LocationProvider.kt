package net.logcat.walkinglooking.location

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

data class Location(val latitude: Double, val longitude: Double)

class LocationProvider(val locationUpdateFrequency: Long = 10000L) {

    private val locations = sequenceOf(
        Location(60.170187, 24.930599),
        Location(60.169418, 24.931618),
        Location(60.169818, 24.932906),
        Location(60.170005, 24.935105),
        Location(60.169108, 24.936210),
        Location(60.168355, 24.934869),
        Location(60.167560, 24.932562),
        Location(60.168254, 24.931532),
        Location(60.169012, 24.930341),
        Location(60.170085, 24.929569)
    )

    val location: Flow<Location> = flow {
        generateSequence { locations }.flatten().forEach {
            emit(it)
            delay(locationUpdateFrequency)
        }
    }
    
}