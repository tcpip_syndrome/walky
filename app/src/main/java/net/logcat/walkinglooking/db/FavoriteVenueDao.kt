package net.logcat.walkinglooking.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface FavoriteVenueDao {

    @Query("select * from FavoriteVenue where id = :venueId")
    suspend fun getFavorite(venueId: String): List<FavoriteVenue>

    suspend fun isFavorite(venueId: String): Boolean {
        return getFavorite(venueId).isNotEmpty()
    }

    @Insert
    suspend fun insertFavorite(favoriteVenue: FavoriteVenue)

    @Delete
    suspend fun deleteFavorite(favoriteVenue: FavoriteVenue)

    suspend fun setFavorite(venueId: String) {
        if (isFavorite(venueId)) {
            deleteFavorite(FavoriteVenue(venueId))
        } else {
            insertFavorite(FavoriteVenue(venueId))
        }
    }
}