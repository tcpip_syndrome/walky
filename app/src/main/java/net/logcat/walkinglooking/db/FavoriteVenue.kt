package net.logcat.walkinglooking.db

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "FavoriteVenue",
        indices = [Index(value = ["id"])])
data class FavoriteVenue(@PrimaryKey val id: String)