package net.logcat.walkinglooking.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [FavoriteVenue::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun favoriteVenueDao(): FavoriteVenueDao
}