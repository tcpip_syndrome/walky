package net.logcat.walkinglooking.util

import android.content.res.Resources
import android.util.TypedValue

fun Resources.dpToPx(dp: Float): Float {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, this.displayMetrics)
}