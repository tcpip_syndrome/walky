App is written in MVVM architecture. Relies heavily on data binding and custom binding adapters.

There are no dependency injection on purpose, in my opinion it hinders code navigation and just as
well can be replaced by setters or constructor parameters.

Commit messages should be explanatory on what was done in which order.

I really did a shortcut on error reporting by showing just one "no internet" error for all kind of
network failures. In general we can differentiate when there is no internet really and when there is
no connection to our server or when there is some other kind of failure during api call or when
response status is not 200.

There was a bump in minSdk version along the way as I've decided not to copy png files for a
favorite icon.